'use strict';
var User = require('../../models/User.js');
var tokens = require('./tokens');
var emailSender = require('../../utils/emailSender');

exports.login = (req, res) => {

    User.findOne({ 'email': req.body.email })
        .then(user => {
            if (!user)
                return res.status(400).send({ message: 'User not found' });

            if (user.status === "INACTIVE"){
                return res.status(400).send({ message: 'User account has not been verify, please open the activation email' });
            }

            user.verifyPassword(req.body.password, function (err, isMatch) {
                if (err)
                    return res.status(500).send({ message: err.message });
                if (!isMatch)
                    return res.status(400).send({ message: 'Password did not match' });
                var token = tokens.generateToken(user);
                return res.status(200).send({ token: token });

            });
        })
        .catch(err => {
            return res.status(500).send({ message: err.message });
        });
}

exports.signup = (req, res) => {

    User.findOne({ 'email': req.body.email })
        .then(existingUser => {
            if (existingUser) {
                return res.status(400).send({ message: 'Email already used' });
            }
            else {
                var newUser = new User();
                newUser.email = req.body.email;
                newUser.password = req.body.password;
                newUser.activateToken = token.generateActivateToken(newUser);
                newUser.activateExpires = Date.now() + 3600000;

                newUser.save()
                    .then(newUser => {
                        return res.status(200).send({ message: 'You user has been created, please check you email accountto activate your user' });
                    })
                    .catch(err => {
                        return res.status(500).send({ message: err.message });
                    });
            }
        })
        .catch(err => {
            return res.status(500).send({ message: err.message });
        });
}

exports.activateAccount = (req, res) => {

    tokens.verifyActivateToken(req.params.token)
    .then(email => {

        User.findOne({ 'email': email,  activateToken: req.params.token})
        .then(user => {

            user.activateToken = null;
            user.activateExpires = null;
            user.status === 'ACTIVE';
            user.save()
                .then(user => {
                    //As this comes from an email, We will need to redirect to the App logon page with a message
                    return res.status(200).send({ message: 'User Account has been activated, you can now logon' });
                })
                .catch(err => {
                    return res.status(500).send({ message: err.message });
                });
        })
        .catch(err => {
            return res.status(500).send({ message: err.message });
        });

}

exports.requestPasswordResetEmail = (req, res) => {
    // 1. Generate a reset JWT Token and save to user table, then email
    User.findOne({ 'email': req.body.user })
        .then(existingUser => {
            if (!existingUser) {
                return res.status(400).send({ message: 'No account with that email address exists.' });
            }
            var token = tokens.generateResetPassToken(existingUser);

            existingUser.resetPasswordToken = token;
            existingUser.resetPasswordExpires = Date.now() + 3600000;

            existingUser.save()
                .then(() => {
                    let emailData = {};
                    emailData.username = existingUser.username;
                    emailData.email = existingUser.email;
                    emailData.link = process.env.CLIENT_BASE_URL + '/#/resetpassword/' + existingUser.resetPasswordToken;

                    emailSender.sendEmail("resetPassword", emailData)
                        .then(result => {
                            return res.status(result.code).send({ message: result.message });
                        })
                        .catch(err => {
                            return res.status(err.code).send({ message: err.message });
                        });
            
                }
            );
                    
        })
        .catch(err => {
            return res.status(500).send(err);
        });
}



exports.resetPassword = (req, res) => {

    User.findOne({ 'email': req.body.user })
        .then(resetUser => {
            if (!resetUser) {
                return res.status(400).send({ message: 'No account with that email address exists.' });
            }
            tokens.verifyResetToken(resetUser.id, req.body.token)
            .then(() => {
                resetUser.resetPasswordToken = null;
                resetUser.resetPasswordExpires = null;
                resetUser.password = req.body.password;
                resetUser.save()
                .then(
                    emailSender.sendEmail(resetUser, "changedPassword")
                        .then(result => {
                            return res.status(result.code).send({ message: result.message });
                        })
                        .catch(err => {
                            return res.status(err.code).send({ message: err.message });
                        })
                );
            })
            .catch(err => {
                return res.status(err.code).send(err);
            });
        })
        .catch(err => {
            return res.status(500).send(err);
        });
}
