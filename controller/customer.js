const Customer   = require('../models/Customer');

exports.customers = (req, res) => {
    console.log(req);
    if(req.query.name){
        oFilter = {'name' : new RegExp(req.query.name, 'i')};
    }else{
        oFilter = {};
    }
    Customer.find(oFilter)
        .then(customers => {
            
            return res.json(customers);
        })
        .catch(err => {
            return err;
        });
    
};

exports.customerById = (req, res) => {
    Customer.findById(req.params.id)
        .then(customer => {
            return res.json(customer);
        })
        .catch(err =>{
            return err;
        }); 
};