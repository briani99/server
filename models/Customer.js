// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');

// Schema for the model
var CustomerSchema = new mongoose.Schema({

    tennant: { type: mongoose.Schema.Types.ObjectId, ref: 'Tennant' },
    
    name: {type: String},
    email: {type: String},
    phone:  {type: String},

    created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
    
    avatar: {type: String}
    
});

// Create a User mongoose model based on the UserSchema
var Customer = mongoose.model('Customer', CustomerSchema);

// Export the User model
module.exports = Customer;