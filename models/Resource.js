// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');

// Schema for the model
var ResourceSchema = new mongoose.Schema({

    tennant: { type: mongoose.Schema.Types.ObjectId, ref: 'Resource' },
    
    name: {type: String},
    type: {type: String},
    description: {type: String},

    created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
    
    image: {type: String}
    
});

// Create a User mongoose model based on the UserSchema
var Resource = mongoose.model('Resource', ResourceSchema);

// Export the User model
module.exports = Resource;