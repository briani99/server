// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');

// Schema for the model
var AppointmentSchema = new mongoose.Schema({

    tennant: { type: mongoose.Schema.Types.ObjectId, ref: 'Tennant' },

    employee_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    resource_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Resource' },
    customer_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Customer' },
    transaction_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Transaction' },
    

    date_time_from: Date,
    date_time_to: Date,
    
    created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now}
    
});

// Before Saving, Check that the resource is available
AppointmentSchema.pre('save', function(next){
    AppointmentSchema.find({resource_id : this.resource_id}, function (err, apts) {
        if (!apts.length){
            next();
        }else{                
            next(new Error("The resource has anothe booking at this time"));
        }
    });
    next();
});

// Before Saving, Check that the resource is available
AppointmentSchema.pre('save', function(next){
    AppointmentSchema.find({employee_id : this.employee_id}, function (err, emp) {
        if (!emp.length){
            next();
        }else{                
            next(new Error("The selected consultant is not available at this time"));
        }
    });
    next();
});

// Create a Appointment mongoose model based on the UserSchema
var Appointment = mongoose.model('Appointment', AppointmentSchema);
// Export the Appointment model
module.exports = Appointment;