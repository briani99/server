// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var client = require('./User');
// Schema for the model

var TranactionSchema = new mongoose.Schema({

    tennant: { type: mongoose.Schema.Types.ObjectId, ref: 'Tennant' },
    
    description : {type: String},
    type        : {type: String},
    item        : {type: String},
    customer    : { type: mongoose.Schema.Types.ObjectId, ref: 'Customer' },
    value       : {type: Number},
    date        : {type: Date, default: Date.now},
      
    created_at  : {type: Date, default: Date.now},
    updated_at  : {type: Date, default: Date.now}
    
});


// Create a User mongoose model based on the UserSchema
var Transaction = mongoose.model('Transaction', TranactionSchema);

// Export the User model
module.exports = Transaction;