// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');

// Schema for the model
var TennantSchema = new mongoose.Schema({
    
    tennant_name: {type: String},
    tennant_admins: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
    
    avatar: {type: String}
    
});


// Create a User mongoose model based on the UserSchema
var Tennant = mongoose.model('Tennant', TennantSchema);

// Export the User model
module.exports = Tennant;