'use strict';
const nodemailer = require('nodemailer');
const Email = require('email-templates');
const path = require('path');

exports.sendEmail = (emailType, emailData) => {

    return new Promise((resolve, reject) => {

        const smtpTransport = nodemailer.createTransport({

            host: process.env.MAIL_HOST,
            port: process.env.MAIL_PORT,
            secure: process.env.MAIL_SECURE,
            service: process.env.MAIL_SERVICE,
            auth: {
                user: process.env.MAIL_USER,
                pass: process.env.MAIL_PASS
            }
        });

        const email = new Email({
            template: path.join(__dirname, 'email_templates', emailType),
            message: {
                from: process.env.MAIL_USER
            },
            // uncomment below to send emails in development/test env:
            send: true,
            transport: smtpTransport
        });

        
        email
            .send({
                template: path.join(__dirname, 'email_templates', emailType),
                message: {
                    to: emailData.email
                },
                locals: emailData
            })
            .then(
                resolve({ code: "200", message: 'An e-mail has been sent' })
            )
            .catch(
                reject({ code: "500", message: 'There has been an error sending the email.' })
            );
    });
}

